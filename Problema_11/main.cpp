/*Programa que gestione los asientos de un cine, este programa los gestiona mediante un arreglo bidimensional
  que tiene dos valores'+' y'-', los cuales se gestionan mediante un menu e iteraciones referenciando a al
  arreglo creado en el main*/

#include <iostream>

using namespace std;

char filas[]={'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O'}; //Char que almacena el nombre de la fila

void inicializar(char asientos[15][20]); //Funcion que recibe un arreglo y lo devuelve iniciado en '-'
void imprimir(char asientos[15][20]);    //Funcion que recibe un arreglo e imprime su contenido de forma organizada
void cambiarEstadoAsiento(char fila, int numAsiento, bool estado,char asientos[15][20]);
    /*Funcion que cambia el estado del asiento de '-' a '+' y viceversa, recibe la fila, el numero de silla
    el estado al que quiere cambiarlo y el arreglo que quiere modificar*/
int buscarFila(char fil); //Funcion que recibe string y lo busca para saber el numero de la fila en el arreglo
int menu(); //Funcion que imprime el menu inicial
void hacerReserva(char asientos[15][20]); //Funcion que realiza la reserva recibe el arreglo que quiere modificar
void cancelarReserva(char asientos[15][20]); //Funcion que cancela la reserva recibe el arreglo que quiere modificar
bool validarEstado(char fila, int numAsiento, char asientos[15][20]); //Funcion que verifica en que estado esta la
                                                                      //ubicación ingresada.

int main(int argc, char** argv) {
    int op=0;
    char asientos[15][20];       //Inicia las variables

    inicializar(asientos);   //Inicia el arreglo en el valor '-', disponible.

    do{
    op=menu(); //Se imprime el menu y recibe la opcion tomada.
    switch(op){
        case 1: //Opcion hacer reserva
            hacerReserva(asientos); //Utiliza la funcion hacer reserva
            imprimir(asientos); //E imprime para que verifique que fue echa

        break;
        case 2: //Opcion que cancela una reserva
            cancelarReserva(asientos); //Utiliza la funcion cancelar reserva
            imprimir(asientos);//imprime el arreglo para que verifique que fue echa exitosamente.
        break;

        case 3: //Funcion ver asientos disponibles
            imprimir(asientos);  //Imprime el arreglo en su estado actual

        break;
        case 4:
            cout << "El programa ha finalizado"<<endl; //Salida

        break;
        default:
            cout << "Ha ingresado una opcion incorrecta"<<endl;  //Entrada no admitida
        break;

    }
    }while(op!=4 || op<0 || op>5); //Seguro de entrada

    return 0;
}

int menu(){
    int op; //Inicio de variables
    do{
    cout <<"Ingrese una opcion:"<<endl;            //
    cout <<"1. Hacer reserva"<< endl;              //
    cout <<"2. Cancelar Reserva"<< endl;           //
    cout <<"3. Ver asientos disponibles "<<endl;   //
    cout <<"4. Sair "<<endl;                       //---> Opciones disponibles
    cin >> op;                                     //
    }while(op <= 0 || op >5);                      //--->Protegidas
                                                   //
    return op;                                     //
}

void hacerReserva(char asientos[15][20]){

    char fila;     //Inicializar las variables
    int asiento;
    imprimir(asientos);  //imprimir los disponibles
    do{
        do{
        cout <<"Ingrese la fila(A-O)"<<endl;    //Entrada de las filas
        cin >> fila;
        if(buscarFila(fila)==-1){
            cout <<"Esta fila no es valida"<<endl;  //Si no es posible
        }
        }while(buscarFila(fila)==-1); //Protege la entrada para que sea valida
        do{
        cout <<"Ingrese el numero del asiento (1-20)"<<endl;  //Entrada del asiento
        cin >> asiento;
        if(asiento <=0 || asiento >20){
            cout <<"ingrese una posicion de asiento correcta"<<endl; //Entrada incorrecta
        }
        }while(asiento <=0 || asiento >20); //Protege la entrada
        if(validarEstado(fila,asiento,asientos)){ //Si es valido.
            cout <<"El asiento no es valido, ya se encuentra ocupado"<<endl; //Si no es valido
        }else{
            cout <<"Se va reservar el asiento "<<fila<<asiento<<endl; //Entrada acceptada
        }
    }while(validarEstado(fila,asiento,asientos)); //Protege esta terminar
    cambiarEstadoAsiento(fila,asiento,true,asientos); //Reserva exitosa usando la funcion cambiar estado.
}

void cancelarReserva(char asientos[15][20]){

    char fila;     //Inicializar las variables
    int asiento;
    imprimir(asientos);  //imprimir los disponibles
    do{
        do{
        cout <<"Ingrese la fila(A-O)"<<endl;    //Entrada de las filas
        cin >> fila;
        if(buscarFila(fila)==-1){
            cout <<"Esta fila no es valida"<<endl;  //Si no es posible
        }
        }while(buscarFila(fila)==-1); //Protege la entrada para que sea valida
        do{
        cout <<"Ingrese el numero del asiento (1-20)"<<endl;  //Entrada del asiento
        cin >> asiento;
        if(asiento <=0 || asiento >20){
            cout <<"ingrese una posicion de asiento correcta"<<endl; //Entrada incorrecta
        }
        }while(asiento <=0 || asiento >20); //Protege la entrada
        if(validarEstado(fila,asiento,asientos)){ //Si es valido.
            cout <<"El asiento no es valido, ya se encuentra ocupado"<<endl; //Si no es valido
        }else{
            cout <<"Se va reservar el asiento "<<fila<<asiento<<endl; //Entrada acceptada
        }
    }while(validarEstado(fila,asiento,asientos)); //Protege esta terminar
    cambiarEstadoAsiento(fila,asiento,false,asientos); //Cancelación exitosa usando la funcion cambiar estado.
}


void inicializar(char asientos[15][20]){

    for(int i=0;i<15;i++){
        for(int j = 0;j<20;j++){  //Recorre todo el arreglo inicializandolo en '-'.
            asientos[i][j]='-';
        }
    }
}

void imprimir(char asientos[15][20]){

    cout<<"\t\tAsientos sala de cine:\n\t    ";

    for(int k=1;k<21;k++) {  // Recorre todas las posiciones
        if(k<10){
            cout << (k)<<"   "; //Imprime la letra hasta el 10 con tres espacios
        }else{
            cout << (k)<<"  ";  //Imprime la letra desde el 11 hasta terminar con dos espacios
        }
    }

    cout <<endl; //Cambio de linea

    for(int i=0;i<15;i++){   //Recorre todas la lineas
        cout <<"\t"<<filas[i]; //Pone un espacio y pone el numero de la fila
        for(int j = 0;j<20;j++){ //Recorre todos los valores y los imprime de la fila
            cout<<"   "<<asientos[i][j]; //Formato de impresión
        }
        cout << endl; //Imprime el salto de linea
    }
}

void cambiarEstadoAsiento(char fila, int numAsiento,bool estado, char asientos[15][20]){//true ocupado - false desocupado

    int fil=buscarFila(fila);  //Busca la ubicacion de la letra en numero

    if(estado){
        asientos[fil][numAsiento-1]='+';   //Cambia el estado a ocupado
    }else{
        asientos[fil][numAsiento-1]='-';   //Cambia de estado a libre
    }

}

bool validarEstado(char fila, int numAsiento, char asientos[15][20]){

    int fil=buscarFila(fila); //Busca la ubicacion de la letra en numero

    if(asientos[fil][numAsiento-1]=='+'){ //verifica si esta ocupado
        return true;
    }else{
        return false; //verifica si esta libre
    }


}

int buscarFila(char fil)
{
    for(int i=0;i<15;i++){ //Recorre buscando la letra
        if(filas[i]==fil){ //Cuando la encuentra devuelve el numero de linea a la que corresponde
            return i;
        }
    }
    return -1; //En caso de no estar devuelve invalido
}

